// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  modules: [
    // "@nuxtjs/supabase",
    "@nuxtjs/tailwindcss",
    "@pinia/nuxt",
    "@vite-pwa/nuxt",
    "nuxt-icon",
  ],
  pages: true,
  runtimeConfig: {
    public: {
      bukcetUrl: process.env.BUCKET_URL,
    },
  },
});
