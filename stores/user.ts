import { defineStore } from "pinia";

interface UserState {
  posts: any[];
  isMenuOverlay: boolean;
  isLogoutOverlay: boolean;
}

export const useUserStore = defineStore("user", {
  state: (): UserState => ({
    posts: [],
    isMenuOverlay: false,
    isLogoutOverlay: false,
  }),
});
